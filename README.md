# 2018/11/10 - Bolivian entities registered in LACNIC

To get the data, just type:

```
make
```

To delete the data:

```
make clean
```

The data created with these scripts on 10 Nov 2018 are located in [D011 - Bolivian records in LACNIC registry](https://data.rednegra.net/2018/11/11/BolivianRecordsInLACNICRegistry.html).

Note: as the Makefile download "intensively" from rdap.lacnic.net, in order to obtain all the file despite the rate limitation, you certainly want to launch make every minute:

```
watch -n 60 make
```
