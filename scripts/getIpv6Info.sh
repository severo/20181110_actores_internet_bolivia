#!/bin/sh

rdapUrl="https://rdap.lacnic.net/rdap/ip/%s"
ipv6=$(echo $1 | sed 's/_/\//' | sed 's/c/:/g')
url=$(printf ${rdapUrl} ${ipv6})
rawJson=$1_raw.json

echo "Download [${url}] and export to [$2] if HTTP code is 200"

curl -w "\n%{http_code}" -s ${url} > ${rawJson}
if [ "$(tail -n 1 ${rawJson})" == "429" ]; then
  rm ${rawJson};
  exit 1;
else
  head -n-1 ${rawJson} > $2;
  rm ${rawJson}
fi
