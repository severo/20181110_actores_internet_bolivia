#!/bin/sh

rdapUrl="https://rdap.lacnic.net/rdap/autnum/%s"
asn=$(echo $1 | sed 's/AS//')
url=$(printf ${rdapUrl} ${asn})
rawJson=$1_raw.json

echo "Download [${url}] and export to [$2] if HTTP code is 200"

curl -w "\n%{http_code}" -s ${url} > ${rawJson}
if [ "$(tail -n 1 ${rawJson})" == "429" ]; then
  rm ${rawJson};
  exit 1;
else
  head -n-1 ${rawJson} > $2;
  rm ${rawJson}
fi
