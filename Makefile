dataDir := data
resourcesListDir := resourcesList
asnInfoDir := asnInfo
ipv4InfoDir := ipv4Info
ipv6InfoDir := ipv6Info
entitiesListDir := entitiesList
entitiesInfoDir := entitiesInfo

asnListFilename := asnList.txt
ipv4ListFilename := ipv4List.txt
ipv6ListFilename := ipv6List.txt
asnInfoFilename := asnInfo.json
ipv4InfoFilename := ipv4Info.json
ipv6InfoFilename := ipv6Info.json
entitiesListFilename := entitiesList.txt
entitiesInfoFilename := entitiesInfo.json

export asnList := $(addsuffix /$(asnListFilename), $(dataDir))
export ipv4List := $(addsuffix /$(ipv4ListFilename), $(dataDir))
export ipv6List := $(addsuffix /$(ipv6ListFilename), $(dataDir))
export asnInfo := $(addsuffix /$(asnInfoFilename), $(dataDir))
export ipv4Info := $(addsuffix /$(ipv4InfoFilename), $(dataDir))
export ipv6Info := $(addsuffix /$(ipv6InfoFilename), $(dataDir))
export entitiesList := $(addsuffix /$(entitiesListFilename), $(dataDir))
export entitiesInfo := $(addsuffix /$(entitiesInfoFilename), $(dataDir))

.PHONY: all
all: $(entitiesInfo)

$(dataDir):
	mkdir -p $(dataDir)

$(asnList) $(ipv4List) $(ipv6List): | $(dataDir)
	$(MAKE) -C $(resourcesListDir)

$(asnInfo): $(asnList)
	$(MAKE) -C $(asnInfoDir)

$(ipv4Info): $(ipv4List)
	$(MAKE) -C $(ipv4InfoDir)

$(ipv6Info): $(ipv6List)
	$(MAKE) -C $(ipv6InfoDir)

$(entitiesList): $(asnInfo) $(ipv4Info) $(ipv6Info)
	$(MAKE) -C $(entitiesListDir)

$(entitiesInfo): $(entitiesList)
	$(MAKE) -C $(entitiesInfoDir)

.PHONY: clean cleanResources cleanAsn cleanIpv4 cleanIpv6 cleanEntitiesList
clean: cleanEntitiesList cleanAsn cleanIpv4 cleanIpv6 cleanResources
	rm -rf data

cleanResources:
	$(MAKE) -C $(resourcesListDir) clean
cleanAsn:
	$(MAKE) -C $(asnInfoDir) clean
cleanIpv4:
	$(MAKE) -C $(ipv4InfoDir) clean
cleanIpv6:
	$(MAKE) -C $(ipv6InfoDir) clean
cleanEntitiesList:
	$(MAKE) -C $(entitiesListDir) clean
cleanEntitiesInfo:
	$(MAKE) -C $(entitiesInfoDir) clean

